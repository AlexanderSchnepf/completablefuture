package at.spengergasse.completeablefuture;

import at.spengergasse.completeablefuture.complete.Completable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.CompletableFuture;

@Controller
public class CompletableController {
    String uri1 =  "https://api.github.com/repos/wycats/merb-core/commits";
    String uri2 =  "https://api.github.com/repos/fastai/fastai/commits";
    String uri3 =  "https://api.github.com/repos/plotly/dash/commits"; 

    @Autowired
    Completable completable;
    @GetMapping("/async")
    public ResponseEntity asynccontroller() throws Exception{
        LocalDateTime date1 = LocalDateTime.now();
       CompletableFuture<Integer> completableFuture1 = completable.asynccount(uri1);
        CompletableFuture<Integer> completableFuture2 = completable.asynccount(uri2);
        CompletableFuture<Integer> completableFuture3 = completable.asynccount(uri3);
        CompletableFuture.allOf(completableFuture1).join();
        CompletableFuture.allOf(completableFuture2).join();
        CompletableFuture.allOf(completableFuture3).join();
        LocalDateTime date2 = LocalDateTime.now();
        long ABC = ChronoUnit.MILLIS.between(date1,date2);
        System.out.println("Milliseconds: "+ABC);
        return ResponseEntity.ok("ABC DEFG");
    }
    @GetMapping("/sync")
    public ResponseEntity synchronouscontroller() throws Exception{
        LocalDateTime date1 = LocalDateTime.now();
        completable.synccount(uri1);
        completable.synccount(uri2);
        completable.synccount(uri3);
        LocalDateTime date2 = LocalDateTime.now();
        long ABC = ChronoUnit.MILLIS.between(date1,date2);
        System.out.println("Milliseconds: "+ABC);
        return ResponseEntity.ok("HIJK LMNOP");
    }
}