package at.spengergasse.completeablefuture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class CompleteablefutureApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompleteablefutureApplication.class, args);
	}

}
