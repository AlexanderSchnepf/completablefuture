package at.spengergasse.completeablefuture.complete;


import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class Completable {


    @Async
    public CompletableFuture<Integer> asynccount(String url) throws JSONException {
        boolean exit = false;
        int page =0;
        int count =1;
        while(exit ==false) {
            page++;
            try {
                CloseableHttpClient httpClient = HttpClientBuilder.create().build();
                HttpGet request = new HttpGet(url + "?page=" + page + "&per_page=100");
                String auth = "SCH17045:ecae8b52b2ec4d3c5304bd3c3387de71dd8ec9da";
                byte[] encodedauth = Base64.encodeBase64(auth.getBytes(StandardCharsets.ISO_8859_1));
                String header = "Basic "+ new String(encodedauth);
                request.setHeader(HttpHeaders.AUTHORIZATION, header);
                CloseableHttpResponse result = httpClient.execute(request);
                String json = EntityUtils.toString(result.getEntity(), "UTF-8");
                JSONArray arr = new JSONArray(json);
                if(arr.length() ==0)
                    exit = true;
                //List<String> list = new ArrayList<String>();
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject commitPackage = arr.getJSONObject(i);
                    String message = commitPackage.getJSONObject("commit").getString("message");
                    System.out.println("Message: '" + message + "'  " + "Anz: " + count);
                    count++;
                }
            } catch (Exception ex) {
                exit = true;
            }
        }

        return CompletableFuture.completedFuture(1);
    }


    public int synccount(String url) throws JSONException {

        boolean exit = false;
        int page =0;
        int count =1;
        while(exit ==false) {
            page++;
            try {
                CloseableHttpClient httpClient = HttpClientBuilder.create().build();
                HttpGet request = new HttpGet(url + "?page=" + page + "&per_page=100");
                String auth = "SCH17045:ecae8b52b2ec4d3c5304bd3c3387de71dd8ec9da";
                byte[] encodedauth = Base64.encodeBase64(auth.getBytes(StandardCharsets.ISO_8859_1));
                String header = "Basic "+ new String(encodedauth);
                request.setHeader(HttpHeaders.AUTHORIZATION, header);
                CloseableHttpResponse result = httpClient.execute(request);
                String json = EntityUtils.toString(result.getEntity(), "UTF-8");
                JSONArray arr = new JSONArray(json);
                if(arr.length() ==0)
                    exit = true;
                //List<String> list = new ArrayList<String>();
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject commitPackage = arr.getJSONObject(i);
                    String message = commitPackage.getJSONObject("commit").getString("message");
                    System.out.println("Message: '" + message + "'  " + "Anz: " + count);
                    count++;
                }
            } catch (IOException ex) {
                exit = true;
            }
        }

        return 1;
    }

}
